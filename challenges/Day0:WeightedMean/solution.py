# URL: https://www.hackerrank.com/challenges/s10-weighted-mean/problem
''''''
input()
elements = list(map(int, input().split()))
weights  = list(map(int, input().split()))

weightssum, elementssum = 0, 0

i, limit = 0, len(elements)
while i < limit:
	elementssum = elementssum + (elements[i] * weights[i])
	weightssum  = weightssum + weights[i]
	
	i += 1

print(f'{elementssum/weightssum:.1f}')

