//URL: https://www.hackerrank.com/challenges/s10-basic-statistics/problem
/*
	Key words: (Mean - Median - Mode - Precision and Scale)

	Precision refers to the number of significant digits in a number. 
	For example, the numbers 123.45 and 0.12345 both have a precision of 5.

	Scale refers to the number of significant digits to the right of the decimal 
	point. For example, the number 123.45 has a scale of 2 decimal places. This 
	term is sometimes misrepresented as precision in documentation.
*/
