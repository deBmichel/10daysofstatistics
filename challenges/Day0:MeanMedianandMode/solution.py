# URL: https://www.hackerrank.com/challenges/s10-basic-statistics/problem
"""
	Topics: 
	Sorting arrays, format to one decimal
"""
import collections
import operator

input()
arr = list(map(int, input().rstrip().split()))
arr.sort()

counts = collections.Counter(arr)
lstCount = list(counts.values())
lstItems = counts.items()

indx_middlea_element = len(arr) // 2
indx_middleb_element = indx_middlea_element - 1

print(f'{sum(arr)/len(arr):.1f}')

if len(arr) % 2 == 1:
	print(arr[indx_middlea_element])
else:
	print(f'{(arr[indx_middlea_element] + arr[indx_middleb_element]) / 2:.1f}')

if lstCount != [lstCount[0]] * len(arr):
	the_max = max(lstCount)
	print (sorted([e for e in lstItems if e[1] == the_max], key=operator.itemgetter(1,0))[0][0])
else:
	print(arr[0])
