# URL: https://www.hackerrank.com/challenges/s10-interquartile-range/problem
'''
	Topics: Sorting arrays, format to one decimal, using generators to reduce code..
'''
def gen(indx, numbers, frecuencies):
	i = 0
	while i < frecuencies[indx]:
		yield numbers[indx]
		i+=1
	
def get_qrtl_val(dataset):
	k_for_len = len(dataset) // 2
	if len(dataset) % 2 == 0 :
		return ( (dataset[(k_for_len) - 1] ) + ( dataset[(k_for_len)]) ) / 2
	else:
		return dataset[(len(dataset) // 2)]

input()
numbers, frecuencies = list(map(int, input().split())), list(map(int, input().split()))
numbers_set = [v for e in range(len(numbers)) for v in gen(e, numbers, frecuencies)]
numbers_set.sort()
k, q1, q3 = len(numbers_set), 0, 0

if k % 2 == 0:
	q1 = get_qrtl_val(numbers_set[0:k//2])
	q3 = get_qrtl_val(numbers_set[k//2:])
else:
	q1 = get_qrtl_val(numbers_set[0:k//2])
	q3 = get_qrtl_val(numbers_set[k//2+1:])

print(f'{q3-q1:.1f}')

