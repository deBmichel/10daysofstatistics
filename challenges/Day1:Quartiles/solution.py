# URL: https://www.hackerrank.com/challenges/s10-quartiles/
''''''
def get_qrtl_val(dataset):
	k_for_len = len(dataset) // 2
	if len(dataset) % 2 == 0 :
		return round(( (dataset[(k_for_len) - 1] ) + ( dataset[(k_for_len)]) ) / 2)
	else:
		return dataset[(len(dataset) // 2)]

k, numbers_set = int(input()), list(map(int, input().split()))
numbers_set.sort()
q1, q2, q3 = 0, 0, 0

if k % 2 == 0:
	q1 = get_qrtl_val(numbers_set[0:k//2])
	q2 = get_qrtl_val([numbers_set[k//2-1], numbers_set[k//2]])
	q3 = get_qrtl_val(numbers_set[k//2:])
else:
	q1 = get_qrtl_val(numbers_set[0:k//2])
	q2 = numbers_set[k//2]
	q3 = get_qrtl_val(numbers_set[k//2+1:])

print(f'{q1}\n{q2}\n{q3}')


