# URL: https://www.hackerrank.com/challenges/s10-standard-deviation/problem
'''
	Defining functions mean median 	and formating in the end .. no individually ...
	Math Square is equal to ** 0.5 and a powerful trick in python ....
	Remember that ..... Lesson : read slow, The algorithm was good designed
	but the function was mean no median ..... read slow

	def median(set_numbers):
	indx_middlea = len(set_numbers) // 2
	indx_middleb = indx_middlea - 1
	if len(set_numbers) % 2 == 1:
		return set_numbers[indx_middlea]
	else:
		return (set_numbers[indx_middlea] + set_numbers[indx_middleb]) / 2

'''
def mean(set_numbers):
	return sum(set_numbers) / len(set_numbers)

def squared_distances(nmbrs, themean):
	return sum([((num - themean) ** 2) for num in nmbrs]) / len(nmbrs)

_, set_numbers = input(), list(map(int, input().split()))
result = squared_distances(set_numbers, mean(set_numbers)) ** 0.5
print(f'{result:.1f}')
